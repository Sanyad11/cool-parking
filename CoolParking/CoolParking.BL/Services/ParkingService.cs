﻿using System;
using System.Collections.Immutable;
using System.Collections.ObjectModel;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private ITimerService _withdrawTimer;
        private ITimerService _logTimer;
        private ILogService _logService;
        private Parking _parking;
       
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        { 
            this._withdrawTimer = withdrawTimer;
            this._logTimer = logTimer;
            this._logService = logService;
            this._parking = Parking.GetInstance();
            this._logTimer.Interval = 1000.0 * Settings.LogInterval;
            this._logTimer.Elapsed += WriteTransactionToLogAndCleanEvent;
            this._withdrawTimer.Interval = 1000.0 * Settings.PayInterval;
            this._withdrawTimer.Elapsed += WithdrawTimer;
            this._logTimer.Start();
            this._withdrawTimer.Start();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (_parking.Vehicles.Count < _parking.Capasity)
            {
                _parking.Vehicles.Add(vehicle.Id, vehicle);
            }
            else
            {
                throw new InvalidOperationException("There is no free places in the parking");
            }
        }

        public void Dispose()
        {
            WriteTransactionToLogAndClean();
            this._logTimer.Stop();
            this._withdrawTimer.Stop();
            _parking?.Dispose();
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return _parking.Capasity;
        }

        public int GetFreePlaces()
        {
            return _parking.Capasity - _parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _parking.Transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(_parking.Vehicles.Values.ToImmutableList());
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (_parking.Vehicles.ContainsKey(vehicleId))
            {
                if (_parking.Vehicles[vehicleId].Balance >= 0)
                {
                    _parking.Vehicles.Remove(vehicleId);
                }
                else
                {
                    throw new InvalidOperationException("You cannot remove vehicle with negative balance!");
                }
            }
            else
            {
                throw new ArgumentException($"Vehicle with id {vehicleId} is not exist and cannot be deleted");
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum > 0 && _parking.Vehicles.ContainsKey(vehicleId))
            {
                _parking.Vehicles[vehicleId].Balance += sum;
            }
            else
            {
                throw new ArgumentException($"Cannot top up the vehicle with id {vehicleId} for the amount: {sum}");
            }

        }

        private void WriteTransactionToLogAndCleanEvent(Object sender, ElapsedEventArgs e)
        {
            WriteTransactionToLogAndClean();
        }
        private void WriteTransactionToLogAndClean()
        {
            string allTransactions = "";
            foreach (var transaction in this.GetLastParkingTransactions())
            {
                allTransactions += $"{transaction}\n";
            }
            if (allTransactions.Contains("\n"))
            {
                allTransactions.Remove(allTransactions.LastIndexOf("\n"));
            }
            _logService.Write(allTransactions);
            this.CleanupTransactions();
        }

        private void CleanupTransactions()
        {
            _parking.Transactions.Clear();
        }

        private void WithdrawTimer(Object sender, ElapsedEventArgs e)
        {
            DateTime time = DateTime.Now;

            foreach (var vehicle in _parking.Vehicles.Values)
            {               
                decimal withdraw = Settings.PayTariffs[vehicle.VehicleType];
                if (vehicle.Balance < withdraw)
                {
                    if (vehicle.Balance <= 0)
                    {
                        withdraw *= Settings.PenaltyCoefficient;
                    }
                    else
                    {
                        withdraw = vehicle.Balance + (withdraw - vehicle.Balance) * Settings.PenaltyCoefficient;
                    }
                }

                vehicle.Balance -= withdraw;
                _parking.Balance += withdraw;
                _parking.Transactions.Add(new TransactionInfo() { Time = time, VehicleId = vehicle.Id, Sum = withdraw }); ;
            }
        }
    }
}