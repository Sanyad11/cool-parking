﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Newtonsoft.Json;

namespace CoolParking.BL.Services
{
    class MenuService : IMenuService
    {
        private IParkingService _parkingService;
        private CoolParking.GetDataFromUser _getDataFromUser;
        private HttpClient _client = new HttpClient();
        private string _baseUrl;

        public MenuService(IParkingService parkingService, CoolParking.GetDataFromUser getDataFromUser, string baseUrl)
        {
            this._parkingService = parkingService;
            this._getDataFromUser = getDataFromUser;
            this._baseUrl = baseUrl;
        }

        public async Task<(string response, bool isFinished)> ProcessRequest(string request)
        {
            string response;
            bool isFinished = false;
            switch (request)
            {
                case "0":
                    response = "Parking closed.";
                    isFinished = true;
                    break;
                case "1":
                    response = await GetParkingBalance();
                    break;
                case "2":
                    response = await GetBalanceDuringCurrentPeriod();
                    break;
                case "3":
                    response = await GetPlacesInfo();
                    break;
                case "4":
                    response = await GetCurrentTransactions();
                    break;
                case "5":
                    response = await TransactionHistory();
                    break;
                case "6":
                    response = await GetVehicles();
                    break;
                case "7":
                    response = await AddNewVehicle();
                    break;
                case "8":
                    response = await RemoveVehicle();
                    break;
                case "9":
                    response = await TopUpVehicle();
                    break;

                default:
                    response = "Wrong input!";
                    break;
            }
            return (response, isFinished);
        }

        public string ShowMenu()
        {
            return "Menu:\n" +
                "1 - Show parking balance\n" +
                "2 - Show parking current period balance\n" +
                "3 - Show count of available and occupied parking places\n" +
                "4 - Show transactions during current period\n" +
                "5 - Show transaction history\n" +
                "6 - Show all vehicles in parking\n" +
                "7 - Add new vehicle to parking\n" +
                "8 - Remove vehicle from parking\n" +
                "9 - Top up one vehicle balance\n" +
                "0 - Exit\n";
        }

        private async Task<string> GetVehicles()
        {
            string result = await GetStringFromUrl($"{_baseUrl}/vehicles");
            return $"Vehicles in parking:\n{result}";
        }

        private async Task<string> TransactionHistory()
        {
            string result = await GetStringFromUrl($"{_baseUrl}/transactions/all");
            return $"Transaction history:\n{result}";
        }

        private async Task<string> GetCurrentTransactions()
        {
            string result = await GetStringFromUrl($"{_baseUrl}/transactions/last");
            return $"List of current period transactions:\n{result}";
        }

        private async Task<string> GetPlacesInfo()
        {
            string result = await GetStringFromUrl($"{_baseUrl}/vehicles");
            int count = JsonConvert.DeserializeObject<IEnumerable<Vehicle>>(result).Count();
            return $"In parking available - {await GetStringFromUrl($"{_baseUrl}/parking/freePlaces")} " +
                $"and occupied - {count} place(s)";
        }

        private async Task<string> GetBalanceDuringCurrentPeriod()
        {
            string result = await GetStringFromUrl($"{_baseUrl}/transactions/last");
            var transactionInfo = JsonConvert.DeserializeObject<IEnumerable<TransactionInfo>>(result);
            decimal currentPeriodBalance = transactionInfo.Sum(transaction => transaction.Sum);
            string message = $"Parking current period balance is {currentPeriodBalance}";
            return message;
        }

        private async Task<string> GetParkingBalance()
        {
            string result = await GetStringFromUrl($"{_baseUrl}/parking/balance");
            return $"Parking balance is {result}";
        }

        private async Task<string> GetStringFromUrl(string requestUri)
        {
            HttpResponseMessage httpResponseMessage = await _client.GetAsync(requestUri);
            return httpResponseMessage.Content.ReadAsStringAsync().Result;

        }

        private async Task<string> AddNewVehicle()
        {
            string vehicleTypeCode = _getDataFromUser("Enter vehicle type:\n" +
                "1 - PassengerCar\n" +
                "2 - Truck\n" +
                "3 - Bus\n" +
                "4 - Motorcycle\n");
            VehicleType vehicleType;
            switch (vehicleTypeCode)
            {
                case "1":
                    vehicleType = VehicleType.PassengerCar;
                    break;
                case "2":
                    vehicleType = VehicleType.Truck;
                    break;
                case "3":
                    vehicleType = VehicleType.Bus;
                    break;
                case "4":
                    vehicleType = VehicleType.Motorcycle;
                    break;
                default:
                    return "Vehicle type incorrect! Try again...";
            }
            string vehicleBalanceInput = _getDataFromUser("Enter vehicle balance:\n");
            decimal vehicleBalance = decimal.Parse(vehicleBalanceInput);
            string vehicleId = _getDataFromUser("Enter vehicle id:\n");
            Vehicle vehicle = new Vehicle(vehicleId, vehicleType, vehicleBalance);
            HttpResponseMessage httpResponseMessage = await _client.PostAsync($"{_baseUrl}/vehicles",
                new StringContent(JsonConvert.SerializeObject(vehicle), Encoding.UTF8, "application/json"));
            if (!httpResponseMessage.IsSuccessStatusCode)
            {
                return await httpResponseMessage.Content.ReadAsStringAsync();
            }
            return $"Vehicle with id {vehicleId} had been created";
        }

        private async Task<string> RemoveVehicle()
        {
            string vehicleId = _getDataFromUser("Enter vehicle id:\n");
            HttpResponseMessage httpResponseMessage = await _client.DeleteAsync($"{_baseUrl}/vehicles/{vehicleId}");
            if (!httpResponseMessage.IsSuccessStatusCode)
            {
                return await httpResponseMessage.Content.ReadAsStringAsync();
            }
            return $"Vehicle with id {vehicleId} had been removed";
        }

        private async Task<string> TopUpVehicle()
        {
            string vehicleId = _getDataFromUser("Enter vehicle id:\n");
            string vehicleBalanceInput = _getDataFromUser("Enter vehicle balance increase:\n");
            decimal vehicleBalanceIncrease = decimal.Parse(vehicleBalanceInput);
            var topUpRequest = new TopUpVehicle(vehicleId, vehicleBalanceIncrease);
            HttpResponseMessage httpResponseMessage = await _client.PutAsync($"{_baseUrl}/transactions/topUpVehicle",
                new StringContent(JsonConvert.SerializeObject(topUpRequest), Encoding.UTF8, "application/json"));
            if (!httpResponseMessage.IsSuccessStatusCode)
            {
                return await httpResponseMessage.Content.ReadAsStringAsync();
            }
            return $"Vehicle with id {vehicleId} balance had been increased";
        }

    }

}
