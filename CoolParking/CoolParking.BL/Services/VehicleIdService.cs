﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Services
{
    public static class VehicleIdService
    {
        private static Random _random = new Random();
        private static Regex _idRegex = new Regex(@"[A-Z][A-Z]-\d{4}-[A-Z][A-Z]");

        public static bool IsValidId(string id)
        {
            if (id.Length == 10 && _idRegex.IsMatch(id))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            return $"{RandomLetter()}{RandomLetter()}-{_random.Next(0, 9999):0000}-{RandomLetter()}{RandomLetter()}";
        }

        private static char RandomLetter()
        {
            return (char)_random.Next(65, 90);
        }
    }
}
