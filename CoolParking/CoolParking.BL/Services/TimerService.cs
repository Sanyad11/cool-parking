﻿using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        private Timer _timer;

        public void Start()
        {
            _timer = new Timer(Interval); 
            _timer.Elapsed += Elapsed;
            _timer.Enabled = true;
        }

        public void Stop()
        {
            _timer.Stop();
        }

        public void Dispose()
        {
            _timer.Dispose();
        }
    }
}
