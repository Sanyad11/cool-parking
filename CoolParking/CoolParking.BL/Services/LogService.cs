﻿using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }

        public LogService(string logPath)
        {
            this.LogPath = logPath;
        }

        public string Read()
        {
            string logs;
            try
            {
                using (var file = new StreamReader(LogPath))
                {
                    logs = file.ReadToEnd();
                }
                return logs;
            }
            catch (FileNotFoundException)
            {
                throw new InvalidOperationException($"Log file {LogPath} not found");
            }
            
        }

        public void Write(string logInfo)
        {
            using (var x = new FileStream(LogPath, FileMode.OpenOrCreate))
            {
            }
            using (var file = new StreamWriter(LogPath, true))
            {
                file.WriteLine(logInfo);
            }
        }
    }
}