﻿using System;
using System.IO;
using System.Reflection;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;

namespace CoolParking.BL
{
    public class CoolParking
    {
        public delegate string GetDataFromUser(string message);

        public static void Main()
        {
            GetDataFromUser getDataFromUser = message => {
                Console.WriteLine(message);
                return Console.ReadLine();
            };
            string timeNow = DateTime.Now.ToString().Replace(" ", "_").Replace(":", "_");
            string logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions{timeNow}.log";
            IParkingService parkingService = new ParkingService(new TimerService(), new TimerService(), new LogService(logFilePath));
            IMenuService menuService = new MenuService(parkingService, getDataFromUser, "http://localhost:30432/api");
            bool isFinished = false;
            while (!isFinished)
            {
                Console.Clear();
                Console.WriteLine(menuService.ShowMenu());
                string request = Console.ReadLine();
                System.Threading.Tasks.Task<(string response, bool isFinished)> task = menuService.ProcessRequest(request);
                task.Wait();
                (string response, bool isFinished) result = task.Result;
                isFinished = result.isFinished;
                Console.WriteLine(result.response);
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
            }
        }
    }
}
