﻿using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking : IDisposable
    {
        private static Parking _instance;

        public decimal Balance { get; internal set; }

        public Dictionary<string, Vehicle> Vehicles { get; }
        public int Capasity { get; }
        public List<TransactionInfo> Transactions { get; }

        private Parking()
        {
            this.Capasity = Settings.ParkingCapacity;
            this.Vehicles = new Dictionary<string, Vehicle>();
            this.Balance = Settings.StartBalance;
            this.Transactions = new List<TransactionInfo>();
        }

        public static Parking GetInstance()
        { 
            if(_instance == null)
            {
                _instance = new Parking();
            }
            return _instance;
        }

        public void Dispose()
        {
            _instance = null;
        }
    }
}
