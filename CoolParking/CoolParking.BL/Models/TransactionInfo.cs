﻿using System;
using System.Globalization;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public decimal Sum { get; set; }

        [JsonProperty("transactionDate")]
        public DateTime Time { get; set; }
        public string VehicleId { get; set; }

        public override string ToString()
        {
            return $"{Time.ToString("M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture)}: {Sum} money withdrawn from vehicle with Id='{VehicleId}'.";
        }
    }
}