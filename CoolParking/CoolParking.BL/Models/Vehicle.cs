﻿using System;
using CoolParking.BL.Services;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public readonly string Id;

        public readonly VehicleType VehicleType;

        public decimal Balance { get; internal set; }
        
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (balance < 0 || !VehicleIdService.IsValidId(id))
            {
                throw new ArgumentException($"Cannot create vehicle with balance = {balance} and id = {id}");
            }
            this.Id = id;
            this.VehicleType = vehicleType;
            this.Balance = balance;
        }

        [JsonConstructor]
        public Vehicle(VehicleType vehicleType, string id, decimal balance)
        {
            this.Id = id;
            this.VehicleType = vehicleType;
            this.Balance = balance;
        }

        public Vehicle(VehicleType vehicleType, decimal balance) 
            : this(VehicleIdService.GenerateRandomRegistrationPlateNumber(), vehicleType, balance)
        {
        }

        public Vehicle()
        {
        }
    }
}