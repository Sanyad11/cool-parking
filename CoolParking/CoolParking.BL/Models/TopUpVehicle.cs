﻿using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    class TopUpVehicle
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("sum")]
        public decimal Sum { get; set; }

        public TopUpVehicle()
        {
        }

        public TopUpVehicle(string id, decimal sum)
        {
            this.Id = id;
            this.Sum = sum;
        }
    }
}
