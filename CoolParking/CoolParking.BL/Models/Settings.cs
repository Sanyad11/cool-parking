﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal StartBalance = 0;
        public static int ParkingCapacity = 10;
        public static int PayInterval = 5;
        public static int LogInterval = 60;
        public static Dictionary<VehicleType, decimal> PayTariffs = new Dictionary<VehicleType, decimal>() 
        {
            { VehicleType.PassengerCar, 2.0m},
            { VehicleType.Truck, 5.0m},
            { VehicleType.Bus, 3.5m},
            { VehicleType.Motorcycle, 1.0m}
        };
        public static decimal PenaltyCoefficient = 2.5m;
    }
}
