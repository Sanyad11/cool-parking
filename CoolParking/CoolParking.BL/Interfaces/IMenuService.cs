﻿using System.Threading.Tasks;

namespace CoolParking.BL.Interfaces
{
    interface IMenuService
    {
        string ShowMenu();
        Task<(string response, bool isFinished)> ProcessRequest(string request);
    }
}
