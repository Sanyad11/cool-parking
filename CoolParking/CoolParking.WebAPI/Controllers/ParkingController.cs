﻿using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [Route("balance")]
        [HttpGet]
        public ActionResult<decimal> GetParkingBalance()
        {
            return Ok(_parkingService.GetBalance());
        }

        [Route("capacity")]
        [HttpGet]
        public ActionResult<int> GetParkingCapacity()
        {
            return Ok(_parkingService.GetCapacity());
        }

        [Route("freePlaces")]
        [HttpGet]
        public ActionResult<int> GetParkingFreePlaces()
        {
            return Ok(_parkingService.GetFreePlaces());
        }
    }
}
