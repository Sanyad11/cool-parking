﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using CoolParking.WebAPI.DTO;
using CoolParking.WebAPI.Services;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : ControllerBase
    {
        private readonly VehicleService _vehicleService;

        public VehiclesController(VehicleService vehicleService)
        {
            this._vehicleService = vehicleService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<VehicleDto>> GetAllVehicles()
        {
            return Ok(_vehicleService.GetAllVehicles());
        }

        [Route("{id}")]
        [HttpGet]
        public ActionResult<VehicleDto> GetVehicleById([FromRoute] string id)
        {
            try
            {
                return Ok(_vehicleService.GetVehicleById(id));
            }
            catch (InvalidOperationException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public ActionResult<VehicleDto> AddVehicle([FromBody] VehicleDto vehicleDto)
        {
            try
            {
                _vehicleService.AddVehicle(vehicleDto);
                return Created("api/vehicles", vehicleDto);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (KeyNotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("{id}")]
        [HttpDelete]
        public ActionResult DeleteVehicleById([FromRoute] string id)
        {
            try
            {
                _vehicleService.DeleteVehicleById(id);
                return NoContent();
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
