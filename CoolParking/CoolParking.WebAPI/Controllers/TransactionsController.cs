﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CoolParking.WebAPI.DTO;
using CoolParking.WebAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {
        private readonly VehicleService _vehicleService;
        private readonly TransactionsService _transactionsService;

        public TransactionsController(VehicleService vehicleService, TransactionsService transactionsService)
        {
            this._vehicleService = vehicleService;
            this._transactionsService = transactionsService;
        }

        [Route("last")]
        [HttpGet]
        public ActionResult<IEnumerable<TransactionDto>> GetLastTransactions()
        {
            return Ok(_transactionsService.GetLastTransactions());
        }

        [Route("all")]
        [HttpGet]
        public ActionResult<string> GetTransactionsHistory()
        {
            try
            {
                return Ok(_transactionsService.GetTransactionsHistory());
            }
            catch (InvalidOperationException ex)
            {
                return NotFound(ex.Message);
            }
        }

        [Route("topUpVehicle")]
        [HttpPut]
        public ActionResult<VehicleDto> TopUpVehicle([FromBody] TopUpVehicleDto topUpVehicleDto)
        {
            try
            {
                return Ok(_vehicleService.TopUpVehicle(topUpVehicleDto.Id, topUpVehicleDto.Sum));
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}
