using System.IO;
using System.Reflection;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using CoolParking.WebAPI.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CoolParking.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddSingleton<IParkingService>(new ParkingService(
                new TimerService(),
                new TimerService(),
                new LogService($@"{ Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) }\Transactions.log")
            ));
            services.AddSingleton<VehicleService, VehicleService>();
            services.AddSingleton<TransactionsService, TransactionsService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
