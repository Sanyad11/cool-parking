﻿using Newtonsoft.Json;

namespace CoolParking.WebAPI.DTO
{
    public class TopUpVehicleDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("sum")]
        public decimal Sum { get; set; }

        public TopUpVehicleDto()
        {
        }

        public TopUpVehicleDto(string id, decimal sum)
        {
            this.Id = id;
            this.Sum = sum;
        }
    }
}
