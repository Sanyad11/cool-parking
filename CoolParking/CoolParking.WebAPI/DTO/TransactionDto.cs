﻿using System;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.DTO
{
    public class TransactionDto
    {
        [JsonProperty("sum")]
        public decimal Sum { get; set; }

        [JsonProperty("transactionDate")]
        public DateTime TransactionDate { get; set; }

        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }

        public TransactionDto() 
        { 
        }

        public TransactionDto(decimal sum, DateTime transactionDate, string vehicleId)
        {
            this.Sum = sum;
            this.TransactionDate = transactionDate;
            this.VehicleId = vehicleId;
        }
    }
}
