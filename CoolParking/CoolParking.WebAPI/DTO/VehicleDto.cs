﻿using CoolParking.BL.Models;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.DTO
{
    public class VehicleDto
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("VehicleType")]
        public VehicleType VehicleType { get; set; }

        [JsonProperty("Balance")]
        public decimal Balance { get; set; }

        public VehicleDto()
        {
        }

        public VehicleDto(string id, VehicleType vehicleType, decimal balance)
        {
            this.Id = id;
            this.VehicleType = vehicleType;
            this.Balance = balance;
        }
    }
}
