﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.WebAPI.DTO;

namespace CoolParking.WebAPI.Services
{
    public class VehicleService
    {
        private readonly IParkingService _parkingService;

        public VehicleService(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        public IEnumerable<VehicleDto> GetAllVehicles()
        {
            return _parkingService.GetVehicles()
                .Select(vehicle => VehicleToVehicleDto(vehicle));
        }

        public VehicleDto GetVehicleById(string id)
        {
            IdValidation(id);

            var vehiclesById = _parkingService.GetVehicles()
                .Where(vehicle => vehicle.Id == id)
                .Select(vehicle => VehicleToVehicleDto(vehicle));

            if (vehiclesById.Count() == 1)
            {
                return vehiclesById.First();
            }
            else
            {
                throw new InvalidOperationException($"Vehicle with id {id} not found!");
            }
        }

        public VehicleDto AddVehicle(VehicleDto vehicleDto)
        {
            IdNotNullValidation(vehicleDto.Id);
            if (!Settings.PayTariffs.ContainsKey(vehicleDto.VehicleType))
            {
                throw new ArgumentException("Wrong vehicle type");
            }
            _parkingService.AddVehicle(VehicleDtoToVehicle(vehicleDto));
            return vehicleDto;
        }

        public VehicleDto TopUpVehicle(string id, decimal sum)
        {
            IdNotNullValidation(id);
            if (sum < 0)
            {
                throw new ValidationException($"Invalid sum {sum}");
            }
            _parkingService.TopUpVehicle(id, sum);
            return this.GetVehicleById(id);
        }

        public void DeleteVehicleById(string id)
        {
            IdValidation(id);
            _parkingService.RemoveVehicle(id);
        }

        private static void IdNotNullValidation(string id)
        {
            if (id == null || !VehicleIdService.IsValidId(id))
            {
                throw new ValidationException("Id is not valid");
            }
        }

        private void IdValidation(string id)
        {
            if (!VehicleIdService.IsValidId(id))
            {
                throw new ValidationException($"Vehicle id {id} is not valid!");
            }
        }

        private Vehicle VehicleDtoToVehicle(VehicleDto vehicleDto)
        {
            return new Vehicle(vehicleDto.Id, vehicleDto.VehicleType, vehicleDto.Balance);
        }

        private VehicleDto VehicleToVehicleDto(Vehicle vehicle)
        {
            return new VehicleDto(vehicle.Id, vehicle.VehicleType, vehicle.Balance);
        }
    }
}
