﻿using System.Collections.Generic;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.DTO;

namespace CoolParking.WebAPI.Services
{
    public class TransactionsService
    {
        private readonly IParkingService _parkingService;

        public TransactionsService(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        public IEnumerable<TransactionDto> GetLastTransactions()
        {
            return _parkingService.GetLastParkingTransactions()
                .Select(transaction => TransactionInfoToTransactionDto(transaction));
        }

        public string GetTransactionsHistory()
        {
            return _parkingService.ReadFromLog();
        }

        private TransactionDto TransactionInfoToTransactionDto(TransactionInfo transactionInfo)
        {
            return new TransactionDto(transactionInfo.Sum, transactionInfo.Time, transactionInfo.VehicleId);
        }
    }
}
